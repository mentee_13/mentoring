﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/*
    У вас  есть два класса Source и Destination. Между классами передается структура InfoData, содержащая данные о пользователе.
    Попытайтесь разорвать связь между классами через InfoData (класс Destination ничего не должен знать об этом типе)
    Оптимизируйте работу с точки зрения быстродействия.
 */
namespace AdvancedTypes_1
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }

    struct InfoData
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    class Source
    {
        internal void CheckAndProceed(List<InfoData> data)
        {
            var dest = new Destination();

            //do something

            dest.ProceedData(data.Select(d => d.FirstName).ToList(), data.Select(d => d.LastName).ToList());
        }
    }

    class Destination
    {
        internal void ProceedData(List<String> firstName, List<String> lastName)
        {
            foreach (var item in firstName)
            {
                //do something
            }
        }
    }
}
