/*
������� 2.3.3

������� ���� ���������� �� ������� Customers, � ������� �������� ������ ���������� �� ����� �� ��������� b � g, �� ��������� �������� BETWEEN. 

*/

use Northwind

SELECT CustomerID, Country
FROM Customers
WHERE (SELECT SUBSTRING(Country, 1, 1)) in ('b','c','d','e','f','g')
Order By Country