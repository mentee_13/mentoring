/*
������� 2.2.1

������� �� ������� Customers ���� ����������, ����������� � USA � Canada. ������ ������� � ������ ������� ��������� IN. 
���������� ������� � ������ ������������ � ��������� ������ � ����������� �������. ����������� ���������� ������� �� ����� ���������� � �� ����� ����������. 

*/

use Northwind

SELECT ContactName as 'Contact Name', Country
FROM Customers
WHERE Country IN ('USA', 'Canada')
ORDER BY ContactName, Country
