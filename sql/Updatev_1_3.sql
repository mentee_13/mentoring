/*Northwind extended update to version 1.2
Author: Dzmitry Ausiyevich
*/
use Northwind
GO

if exists (select * from sysobjects where id = object_id('dbo.Cards') and sysstat & 0xf = 3)
	drop table "dbo"."Cards"
GO

CREATE TABLE Cards (
	"CardID" nchar (16) NOT NULL ,
	"EndDate" "datetime" NOT NULL ,
	"EmployeeID" "int" NULL ,
	CONSTRAINT "PK_Cards" PRIMARY KEY  CLUSTERED 
	(
		"CardID"
	),
	CONSTRAINT "FK_Cards_Employees" FOREIGN KEY 
	(
		"EmployeeID"
	) REFERENCES "dbo"."Employees" (
		"EmployeeID"
	)
)
GO

if exists (select * from sysobjects where id = object_id('dbo.Region') and sysstat & 0xf = 3) 
		and exists (select * from sysobjects where id = OBJECT_ID('dbo.Territories') and sysstat & 0xf = 3)
BEGIN
			EXEC sp_rename 'Region', 'Regions'
			alter table Territories drop constraint FK_Territories_Region
			ALTER TABLE Territories
			ADD CONSTRAINT [FK_Territories_Regions] FOREIGN KEY 
			(
				[RegionID]
			) REFERENCES [dbo].[Regions] (
				[RegionID]
			)
END
GO