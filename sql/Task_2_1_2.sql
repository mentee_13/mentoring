/*
������� 2.1.2

�������� ������, ������� ������� ������ �������������� ������ �� ������� Orders. � ����������� ������� ���������� ��� ������� ShippedDate 
������ �������� NULL ������ �Not Shipped� (������������ ��������� ������� CAS�). ������ ������ ���������� ������ ������� OrderID � ShippedDate. 

*/

use Northwind

SELECT OrderID, CASE WHEN ShippedDate IS NULL THEN 'Not Shipped' END ShippedDate
FROM Orders
where ShippedDate IS NULL
