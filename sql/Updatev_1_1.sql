/*Northwind extended update to version 1.1
Author: Dzmitry Ausiyevich
*/
use Northwind
GO

if exists (select * from sysobjects where id = object_id('dbo.Cards') and sysstat & 0xf = 3)
	drop table "dbo"."Cards"
GO

CREATE TABLE Cards (
	"CardID" nchar (16) NOT NULL ,
	"EndDate" "datetime" NOT NULL ,
	"EmployeeID" "int" NULL ,
	CONSTRAINT "PK_Cards" PRIMARY KEY  CLUSTERED 
	(
		"CardID"
	),
	CONSTRAINT "FK_Cards_Employees" FOREIGN KEY 
	(
		"EmployeeID"
	) REFERENCES "dbo"."Employees" (
		"EmployeeID"
	)
)
GO