/*
������� 2.4.1

������������� ��������� LIKE 
1. � ������� Products ����� ��� �������� (������� ProductName), ��� ����������� ��������� 'chocolade'. 
��������, ��� � ��������� 'chocolade' ����� ���� �������� ���� ����� 'c' � �������� - ����� ��� ��������, ������� ������������� ����� �������.
*/

use Northwind

SELECT ProductName
FROM Products
WHERE ProductName like '%cho%olade%'
