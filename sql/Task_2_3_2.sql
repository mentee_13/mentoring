/*
������� 2.3.2

������� ���� ���������� �� ������� Customers, � ������� �������� ������ ���������� �� ����� �� ��������� b � g. 
������������ �������� BETWEEN. ���������, ��� � ���������� ������� �������� Germany. 
������ ������ ���������� ������ ������� CustomerID � Country � ������������ �� Country. 

*/

use Northwind

SELECT CustomerID, Country
FROM Customers
WHERE (SELECT SUBSTRING(Country, 1, 1)) between 'b' and 'g'
Order By Country