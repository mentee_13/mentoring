/*
������� 2.2.2

������� �� ������� Customers ���� ����������, �� ����������� � USA � Canada. ������ ������� � ������� ��������� IN. 
���������� ������� � ������ ������������ � ��������� ������ � ����������� �������. ����������� ���������� ������� �� ����� ����������. 

*/

use Northwind

SELECT ContactName as 'Contact Name', Country
FROM Customers
WHERE Country NOT IN ('USA', 'Canada')
ORDER BY ContactName
