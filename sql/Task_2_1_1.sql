/*
������� 2.1.1

������� � ������� Orders ������, ������� ���� ���������� ����� 6 ��� 1998 ���� (������� ShippedDate) ������������ 
� ������� ���������� � ShipVia >= 2. ������ ������ ���������� ������ ������� OrderID, ShippedDate � ShipVia. 

*/

use Northwind

SELECT OrderID, ShippedDate, ShipVia
FROM Orders
WHERE ShipVia >=2 and ShippedDate >= '1998-05-06'
