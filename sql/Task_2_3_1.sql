/*
������� 2.3.1

������� ��� ������ (OrderID) �� ������� Order Details (������ �� ������ �����������), 
��� ����������� �������� � ����������� �� 3 �� 10 ������������ � ��� ������� Quantity � ������� Order Details. 
������������ �������� BETWEEN. ������ ������ ���������� ������ �������

*/

use Northwind

SELECT Distinct OrderID
FROM [Order Details]
WHERE Quantity between 3 and 10
