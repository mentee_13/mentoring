/*
������� 2.1.3

������� � ������� Orders ������, ������� ���� ���������� ����� 6 ��� 1998 ���� (ShippedDate) �� ������� ��� ���� ��� ������� ��� �� ����������. 
� ������� ������ ������������ ������ ������� OrderID (������������� � Order Number) � ShippedDate (������������� � Shipped Date). 
� ����������� ������� ���������� ��� ������� ShippedDate ������ �������� NULL ������ �Not Shipped�, ��� ��������� �������� ���������� ���� � ������� �� ���������. 

*/

use Northwind

SELECT OrderID as 'Order Number', CASE WHEN ShippedDate IS NULL THEN 'Not Shipped' ELSE CAST(ShippedDate AS CHAR(30)) END as 'Shipped Date'
FROM Orders
WHERE ShippedDate > '1998-05-06' or ShippedDate IS NULL
