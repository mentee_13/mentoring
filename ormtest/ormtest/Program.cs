﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModels;
using LinqToDB;

namespace ormtest
{
    class Program
    {
        static void Main(string[] args)
        {
           /* List<string> territories = new List<string>()
            {
                "01581",
                "02116"
            };

            InsertNewEmploee("Ivan", "Ivanov", territories);*/

            //MoveProductsToCategorie(2, 1);

        }


        //Список продуктов с категорией и поставщиком
        static void ProductsWithCategoryAndSeller() {
            using (var db = new NorthwindDB())
            {
                var q =
                    from c in db.Products
                    where c.CategoryID != null && c.SupplierID != null
                    select c;

                foreach (var c in q)
                    Console.WriteLine(c.ProductName);
                Console.ReadLine();
            }            
        }

        //Cписок сотрудников с указанием региона, за который они отвечают
        static void EmploeesWithRegions() {
            using (var db = new NorthwindDB())
            {
                var q =
                    from e in db.Employees
                    select new { 
                        e.FirstName,
                        e.LastName,
                        e.Region
                    };

                foreach (var e in q)
                    Console.WriteLine(String.Format("{0}, {1}: {2}", e.FirstName, e.LastName, e.Region));
                Console.ReadLine();
            }              
        }

        //Статистики по регионам: количества сотрудников по регионам 
        static void CountEmploeeByRegion() {
            int count = 0;
            using (var db = new NorthwindDB())
            {
                var q =
                    from e in db.Employees
                    group new { e.Region } by e.Region into r
                    select r;

                foreach (var e in q.ToList())
                {
                    Console.WriteLine(String.Format("Region {0} have {1} emploees", e.Key, e.ToList().Count));
                }
                Console.ReadLine();
            }               
        }

        //Списка «сотрудник – с какими грузоперевозчиками работал» (на основе заказов)
        static void EmploeeWithShippers()
        {
            int count = 0;
            using (var db = new NorthwindDB())
            {
                var q =
                    from o in db.Orders
                    join e in db.Employees on o.EmployeeID equals e.EmployeeID
                    let name = e.FirstName + ' ' + e.LastName
                    group new { o.ShipName } by name into item
                    select item;

                foreach (var e in q.ToList())
                {
                    Console.WriteLine();
                    Console.WriteLine(String.Format("Emploee {0} works with:", e.Key));
                    Console.WriteLine();
                    foreach (var ship in e) {
                        Console.WriteLine(ship.ShipName);    
                    }
                }
                Console.ReadLine();
            }
        }

        static void InsertNewEmploee(string FirstName, string LastName, List<string> territories)
        {
            int numOfTerritories = 0;
            Random rnd = new Random();

            Employee e = new Employee();
            e.FirstName = FirstName;
            e.LastName = LastName;

            using (var db = new NorthwindDB())
            {
                e.EmployeeID = Convert.ToInt32(db.InsertWithIdentity(e));
                numOfTerritories = rnd.Next(1, 4);

                foreach(string territory in territories){
                    EmployeeTerritory et = new EmployeeTerritory();
                    et.EmployeeID = e.EmployeeID;
                    et.TerritoryID = territory;
                    db.Insert(et);
                }
            }
        }

        static void MoveProductsToCategorie(int? oldCategoryID, int? newCategoryID) {
            List<Product> products;
            
            using (var db = new NorthwindDB()) 
            {
                var q = from prod in db.Products
                        where prod.CategoryID == oldCategoryID
                        select prod;
                products = q.ToList();
            }

            using (var db = new NorthwindDB()) {
                foreach (Product prod in products)
                {
                    prod.CategoryID = newCategoryID;
                    db.Update(prod);
                }
            }
        }


    }
}
