﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

/*
 Напишите атрибут, который можно применить только к классу. 
 Атрибут содержит информацию об авторе кода (имя, e-mail). 
 Напишите функционал, который позволит вывести эту информацию в консоль.
 */

namespace BCL1
{

    [AttributeUsage(AttributeTargets.Class)]
    public class DeveloperAttribute : Attribute
    {
        private string _name;
        private string _email;
        public DeveloperAttribute(string name, string email)
        {
            _name = name;
            _email = email;
        }

        public string Name
        {
            get
            {
                return _name;
            }
        }

        public string Email
        {
            get
            {
                return _email;
            }
        }

        public override string ToString()
        {
            string value = "Developer : " + Name + Environment.NewLine;
            value += "Email : " + Email;
            return value;
        }
    }

    [Developer("Dmzitry Ausiyevich", "dzmitry_ausiyevich@epam.com")]
    class SampleClass
    {

    }

    class MainClass
    {
        private static void ShowAttributes(MemberInfo member)
        {
            Console.WriteLine("Attributes for : " + member.Name);
            foreach (object attribute in member.GetCustomAttributes(true))
            {
                Console.WriteLine(attribute);
            }
        }

        public static void Main()
        {

            ShowAttributes(typeof(SampleClass));
            Console.ReadLine();
        }
    }
}
