﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
Напишите метод для сортировки массива строк в независимости от региональных стандартов пользователя. Использование Linq запрещено. 
 */

namespace BLC2
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] sampleArray = new string[] { "asd", "gfdg", "qwed", "Aaa", "aaA" };
            Array.Sort(sampleArray, StringComparer.InvariantCulture);
            foreach (string s in sampleArray)
            {
                Console.WriteLine(s);
            }

            Console.ReadLine();
        }
    }
}
